# Guess random birthday
from random import randint

# Ask player name
player_name = input("Hi! What is your name? - ")


for guess_birthday in range(5):
    month = randint(1, 12)
    year = randint(1924, 2004)
    print("Hi", player_name + ",",  "is your birthday", month, "/", year, "?")
    response = input("Is this correct? - ")
    if guess_birthday == 4:
        print("I have other things to do. Good bye.")
    if response == "yes":
        print("I guessed right!")
        break
    elif response == "no":
        if guess_birthday < 4:
            print("Drat! Lemme try again!")
